/*******************************************************************************
 * Copyright (c) 2014 Ericsson and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Marc Khouzam (Ericsson) - Initial API and implementation
 *******************************************************************************/

package org.eclipse.cdt.dsf.mi.service.command.output;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * '-gdb-version' Show version information for gdb.
 *
 * sample output:
 *
 * -gdb-version
 * ~"GNU gdb (Ubuntu 7.7-0ubuntu3.1) 7.7\n"
 * ~"Copyright (C) 2014 Free Software Foundation, Inc.\n"
 * ~"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.  Type \"show copying\"\nand \"show warranty\" for details.\n"
 * ~"This GDB was configured as \"x86_64-linux-gnu\".\nType \"show configuration\" for configuration details."
 * ~"\nFor bug reporting instructions, please see:\n"
 *  ~"<http://www.gnu.org/software/gdb/bugs/>.\n"
 * ~"Find the GDB manual and other documentation resources online at:\n<http://www.gnu.org/software/gdb/documentation/>.\n"
 * ~"For help, type \"help\".\n"
 * ~"Type \"apropos word\" to search for commands related to \"word\".\n"
 * ^done
 *
 * @since 4.6
 */
public class MIGDBVersionInfo extends MIInfo {

	private String fVersion;
	private String fFullOutput;

	public MIGDBVersionInfo(MIOutput record) {
		super(record);
		parse();
	}

	protected void parse() {
		if (isDone()) {
			MIOutput out = getMIOutput();
			MIOOBRecord[] records = out.getMIOOBRecords();
			StringBuilder builder = new StringBuilder();
			for (MIOOBRecord rec : records) {
				if (rec instanceof MIConsoleStreamOutput) {
					MIStreamRecord o = (MIStreamRecord) rec;
					builder.append(o.getString());
				}
			}
			fFullOutput = builder.toString();
			fVersion = parseVersion(fFullOutput);
		}
	}

	protected String parseVersion(String output) {
		return getGDBVersionFromText(output);
	}

	public String getFullOutput() {
		return fFullOutput;
	}

	public String getVersion() {
		return fVersion;
	}

	/**
	 * Find gdb version info from a string object which is supposed to
	 * contain output text of "gdb --version" command.
	 *
	 * @param versionOutput
	 * 		output text from "gdb --version" command .
	 * @return
	 * 		String representation of version of gdb such as "6.8" on success;
	 *      empty string otherwise.
	 * @since 2.0
	 */
	public static String getGDBVersionFromText(String versionOutput) {
		String version = "";//$NON-NLS-1$

		// These are the GDB version patterns I have seen up to now
		// The pattern works for all of them extracting the version of 6.8.50.20080730
		// GNU gdb 6.8.50.20080730
		// GNU gdb (GDB) 6.8.50.20080730-cvs
		// GNU gdb (Ericsson GDB 1.0-10) 6.8.50.20080730-cvs
		// GNU gdb (GDB) Fedora (7.0-3.fc12)
		// GNU gdb Red Hat Linux (6.3.0.0-1.162.el4rh)
		// GNU gdb (GDB) STMicroelectronics/Linux Base 7.4-71 [build Mar  1 2013]

		Pattern pattern = Pattern.compile(" gdb( \\(.*?\\))? (\\D* )*\\(?(\\d*(\\.\\d*)*)", Pattern.MULTILINE); //$NON-NLS-1$

		Matcher matcher = pattern.matcher(versionOutput);
		if (matcher.find()) {
			version = matcher.group(3);
			// Temporary for cygwin, until GDB 7 is released
			// Any cygwin GDB staring with 6.8 should be treated as plain 6.8
			if (versionOutput.toLowerCase().contains("cygwin") && //$NON-NLS-1$
					version.startsWith("6.8")) { //$NON-NLS-1$
				version = "6.8"; //$NON-NLS-1$
			}
		}

		return version;
	}
}
