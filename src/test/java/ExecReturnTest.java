import org.eclipse.cdt.dsf.mi.service.command.output.*;
import org.junit.Assert;
import org.junit.Test;

public class ExecReturnTest {
    @Test
    public void basic() {
        String line = "^done,frame={level=\"0 \",func=\"callee3\"," +
                "args=[{name=\"strarg\",value=\"0x11940 \\\"A string argument.\\\"\"}]," +
                "file=\"../../../devo/gdb/testsuite/gdb.mi/basics.c\",line=\"18\"}" +
                "(gdb)";

        MIParser parser = new MIParser();
        MIParser.RecordType recordType = parser.getRecordType(line);
        Assert.assertEquals(recordType, MIParser.RecordType.ResultRecord);

        MIResultRecord record = parser.parseMIResultRecord(line);
        MIResult[] results = record.getMIResults();
        Assert.assertEquals(results.length, 1);
        Assert.assertEquals(results[0].getVariable(), "frame");
    }
}
