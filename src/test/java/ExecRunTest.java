import org.eclipse.cdt.dsf.mi.service.command.output.MIOOBRecord;
import org.eclipse.cdt.dsf.mi.service.command.output.MIParser;
import org.eclipse.cdt.dsf.mi.service.command.output.MIResultRecord;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ExecRunTest {
    @Test
    public void testRunning() {
        String log = "^running\n" +
                "(gdb)\n" +
                "@Hello world\n" +
                "*stopped,reason=\"breakpoint-hit\",bkptno=\"2\",frame={func=\"foo\",args=[],file=\"hello.c\",line=\"13\"}\n" +
                "(gdb)";
        String[] lines = log.split("\\R");
        List<Object> records = new ArrayList<>();
        for (String line : lines) {
            MIParser parser = new MIParser();
            MIParser.RecordType type = parser.getRecordType(line);
            switch (type) {
                case ResultRecord:
                    MIResultRecord resultRecord = parser.parseMIResultRecord(line);
                    records.add(resultRecord);
                    break;

                case OOBRecord:
                    MIOOBRecord oobRecord = parser.parseMIOOBRecord(line);
                    records.add(oobRecord);
                    break;
            }
        }

        Assert.assertEquals(records.size(), 3);
    }
}
