import org.eclipse.cdt.dsf.mi.service.command.output.*;
import org.junit.Assert;
import org.junit.Test;

public class VarCreateTest {
    @Test
    public void testIntVar() {
        MIParser parser = new MIParser();
        String line = "^done,name=\"var1\",numchild=\"0\",value=\"11\",type=\"int\"";
        MIResultRecord record = parser.parseMIResultRecord(line);
        MIOutput output = new MIOutput(record, null);
        MIVarCreateInfo info = new MIVarCreateInfo(output);
        Assert.assertEquals(info.getMIVar().getVarName(), "var1");
        Assert.assertEquals(info.getMIVar().getNumChild(), 0);
        Assert.assertEquals(info.getMIVar().getValue(), "11");
        Assert.assertEquals(info.getMIVar().getType(), "int");
    }
}
