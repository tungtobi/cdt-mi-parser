import org.eclipse.cdt.dsf.mi.service.command.output.*;
import org.junit.Assert;
import org.junit.Test;

public class VarListChildrenTest {
    @Test
    public void basic() {
        String line = "^done,numchild=\"6\",children={child={name=\"var2.0\",exp=\"0\",numchild=\"0\",type=\"char\"},child={name=\"var2.1\",exp=\"1\",numchild=\"0\",type=\"char\"},child={name=\"var2.2\",exp=\"2\",numchild=\"0\",type=\"char\"},child={name=\"var2.3\",exp=\"3\",numchild=\"0\",type=\"char\"},child={name=\"var2.4\",exp=\"4\",numchild=\"0\",type=\"char\"},child={name=\"var2.5\",exp=\"5\",numchild=\"0\",type=\"char\"}}";

        MIParser parser = new MIParser();
        MIParser.RecordType recordType = parser.getRecordType(line);
        Assert.assertEquals(recordType, MIParser.RecordType.ResultRecord);

        MIResultRecord record = parser.parseMIResultRecord(line);
        MIOutput output = new MIOutput(record, null);
        MIVarListChildrenInfo info = new MIVarListChildrenInfo(output);


        Assert.assertEquals(info.getMIVars().length, 6);
    }
}
