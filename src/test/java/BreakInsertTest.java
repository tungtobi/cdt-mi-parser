import org.eclipse.cdt.dsf.mi.service.command.output.*;
import org.junit.Assert;
import org.junit.Test;

public class BreakInsertTest {
    @Test
    public void testMultipleLocations() {
        MIParser parser = new MIParser();
        String line = "^done," +
                "bkpt={number=\"2\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"<MULTIPLE>\",times=\"0\",original-location=\"for_dfs_test.for.akaignore.c:22\"}," +
                "{number=\"2.1\",enabled=\"y\",addr=\"0x00401984\",func=\"basic\",file=\"D:/Computer_science/LAB_PROJECT/Test_C/for_dfs_test/for_dfs_test.for.akaignore.c\",fullname=\"D:\\\\Computer_science\\\\LAB_PROJECT\\\\Test_C\\\\for_dfs_test\\\\for_dfs_test.for.akaignore.c\",line=\"22\",thread-groups=[\"i1\"]}," +
                "{number=\"2.2\",enabled=\"y\",addr=\"0x00401a09\",func=\"basic\",file=\"D:/Computer_science/LAB_PROJECT/Test_C/for_dfs_test/for_dfs_test.for.akaignore.c\",fullname=\"D:\\\\Computer_science\\\\LAB_PROJECT\\\\Test_C\\\\for_dfs_test\\\\for_dfs_test.for.akaignore.c\",line=\"22\",thread-groups=[\"i1\"]}," +
                "time={wallclock=\"0.00000\",user=\"0.00000\",system=\"0.00000\"}";
        MIResultRecord record = parser.parseMIResultRecord(line);
        MIOutput output = new MIOutput(record, null);
        MIBreakInsertInfo info = new MIBreakInsertInfo(output);
        Assert.assertEquals(info.getMIBreakpoints().length, 3);
    }
}
